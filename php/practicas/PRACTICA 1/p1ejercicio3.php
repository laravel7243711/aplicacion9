<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ejercio 3</title>
</head>
<body>
    <h2>Método 1</h2>
    <?php
    // mezclamos html y php
    echo "<p> Hola mundo </p>";
    ?>  
    <h2>Método 2</h2>
    <p>
        <?php
        // solo coloco php
        echo "Hola mundo";
        ?>        
    </p>
    <h2>Método 3</h2>
    <?php
    echo "</p>";
    ?>
    <h2>Podemos utilizar el modo contraido del hecho</h2>  
    <p>
        <?= "Hola mundo"?>
    </p>
    
</body>
</html>