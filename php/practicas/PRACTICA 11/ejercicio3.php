<?php

/**
 * Genera un array de colores aleatorios.
 * y los almacena en un array
 * @param int $num La cantidad de colores a generar.
 * @return array Un array de valores de colores RGB.
 */

function generaColores($num){
    // array donde se almacena el resultado
    $colores=array();
    // bucle para rellenar el array
    for($i=0;$i<$num;$i++){
        $colores[$i]=array(mt_rand(0,255),mt_rand(0,255),mt_rand(0,255));
    }

    // devolver el array
    return $colores;
}
    


?>