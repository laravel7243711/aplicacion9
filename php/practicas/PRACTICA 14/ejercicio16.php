<?php

// crear una una variable global denominada resultado
// crear una función que le pasas dos numeros y te almacena
// el resultado en la variable global resultado

function suma($num1, $num2) {
    global $resultado;
    $resultado = $num1 + $num2;
}   

// Llamamos a la función para imprimir los números
suma(1, 2);

echo $resultado;



