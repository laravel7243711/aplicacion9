<?php
// crear una función que imprima dos numeros desde dos argumentos pasados a la funcion
// por ejemplo(1,100) imprime los numeros que hay entre uno y 100

function imprimir_numeros($num1, $num2) {
    for ($i = $num1; $i <= $num2; $i++) {
        echo $i . " ";
    }
}

// Llamamos a la función para imprimir los números
imprimir_numeros(1, 100);