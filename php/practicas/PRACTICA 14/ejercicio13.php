<?php

// crear una función que le pasas un array con tres colores y te imprime
// tes div de 100px de alto cada uno de ellos  con el color de fondo del array 
// pasado como argumento

function imprimeDivs($array) {
    for ($i = 0; $i < count($array); $i++) {
        echo "<div style='background-color: " . $array[$i] . "; height: 100px;'></div>";
    }
}

// Llamamos a la función para imprimir los divs
imprimeDivs(["red", "green", "blue"]);

