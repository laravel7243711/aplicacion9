<?php

// crear una función que le pasas dos numeros y te imprima la suma

function sumar_dos_numeros($num1, $num2) {
    return $num1 + $num2;
}

// Llamamos a la función para sumar los numeros

echo sumar_dos_numeros(10, 20);

// crear una función que le pasas tres numeros y te imprima la suma

function sumar_tres_numeros($num1, $num2, $num3) {
    return $num1 + $num2 + $num3;
}
// llamamos a la función para sumar los números
echo sumar_tres_numeros(10, 20, 30);
