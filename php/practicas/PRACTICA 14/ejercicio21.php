<?php
function operar_con_array($numeros, $operacion) {
    // Verificamos si el array tiene al menos 2 elementos
    if (count($numeros) < 2) {
        return "El array debe contener al menos 2 números.";
    }

    // Realizamos la operación según el argumento proporcionado
    switch ($operacion) {
        case 'suma':
            return array_sum($numeros);
        case 'producto':
            return array_product($numeros);
        case 'resta':
            // Restamos los elementos a partir del segundo
            $resultado = $numeros[0];
            for ($i = 1; $i < count($numeros); $i++) {
                $resultado -= $numeros[$i];
            }
            return $resultado;
        default:
            return "Operación no válida. Debe ser 'suma', 'producto' o 'resta'.";
    }
}

// Ejemplo de uso
$mis_numeros = array(10, 5, 3, 2);
$operacion_suma = 'suma';
$operacion_producto = 'producto';
$operacion_resta = 'resta';

$resultado_suma = operar_con_array($mis_numeros, $operacion_suma);
$resultado_producto = operar_con_array($mis_numeros, $operacion_producto);
$resultado_resta = operar_con_array($mis_numeros, $operacion_resta);

echo "Resultado de la suma: " . $resultado_suma . "\n";
echo "Resultado del producto: " . $resultado_producto . "\n";
echo "Resultado de la resta: " . $resultado_resta . "\n";

