<?php
// crear una función que le pasas un string como argumento
// y te devuelve un array con las vocales que tiene el string


function obtener_vocales($cadena) {
    // Convertimos el string a minúsculas para evitar problemas con mayúsculas
    $cadena = strtolower($cadena);
    
    // Definimos un array para almacenar las vocales
    $vocales = array();
    
    // Recorremos cada caracter del string
    for ($i = 0; $i < strlen($cadena); $i++) {
        $caracter = $cadena[$i];
        // Verificamos si el caracter es una vocal
        if (in_array($caracter, array('a', 'e', 'i', 'o', 'u'))) {
            // Agregamos la vocal al array
            $vocales[] = $caracter;
        }
    }
    
    // Devolvemos el array de vocales
    return $vocales;
}

// Ejemplo de uso
$mi_string = "Hola, este es un ejemplo de string con algunas vocales.";
$vocales_en_string = obtener_vocales($mi_string);

// Imprimimos las vocales encontradas
echo "Las vocales en el string son: " . implode(", ", $vocales_en_string);
