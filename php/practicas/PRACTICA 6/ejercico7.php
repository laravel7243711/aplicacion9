<?php
// crear un programa que nos cree tres variables denominadas a, b, c
// asignar tres numeros aleatorios a las variables
// comparar si a es mayor que b
// comparar si a es mayor que c
// comparar si b es mayor que c
// el programa debe decir cual de los tres es el mayor
$a = rand(1,100);
$b = rand(1,100);
$c = rand(1,100);

echo "<br>{$a}</br>";
echo "<br>{$b}</br>";
echo "<br>{$c}</br>";

if ($a > $b && $a > $c) {
    echo "El mayor es $a";  
} elseif ($b > $a && $b > $c) {
    echo "El mayor es $b";
} elseif ($c > $a && $c > $b) {
    echo "El mayor es $c";
};

?>
