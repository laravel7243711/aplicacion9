<?php
// crear un array con 10 numeros aleatorios
$array = [];

for ($i = 0; $i < 10; $i++) {
    $array[] = rand(1, 100);
}

// mostrar el array con un for
for ($i = 0; $i < count($array); $i++) {
    echo "<br> \$array[$i] = $array[$i]";
}
// mostrar el array con un foreach
foreach($array as $key => $value) {
    echo "<br> \$array[$key] = $value";
}
?>

