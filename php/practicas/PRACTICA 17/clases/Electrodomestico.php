<?php
namespace clases;

class Electrodomestico
{
// propiedades
// tipo marca y potencia
public string $tipo;
public string $marca;
public float $potencia;

public int $consumo;

// hacer un constructor que reciba los parametros tipo marca y potencia
// y los asigne a cada propiedad

public function __construct(string $tipo, string $marca, float $potencia)
{
    $this->marca = $marca;
    $this->potencia = $potencia;
    $this->tipo = $tipo;
}
// programar un toString que devuelva el tipo marca y la potencia
public function __toString(): string
{
    return
    "La marca es: " . $this->marca 
     ."El tipo es: " . $this->tipo
     . " y la potencia es: " . $this->potencia;
}


/**
 * Get the value of tipo
 */
public function getTipo(): string
{
return $this->tipo;
}

/**
 * Set the value of tipo
 */
public function setTipo(string $tipo): self
{
$this->tipo = $tipo;

return $this;
}

/**
 * Get the value of marca
 */
public function getMarca(): string
{
return $this->marca;
}

/**
 * Set the value of marca
 */
public function setMarca(string $marca): self
{
$this->marca = $marca;

return $this;
}
/** 
 * Set the value potencia
 * @param float $potencia
 **/
public function setPotencia(float $potencia): void
{
    $this->potencia = $potencia;
}
/** 
 * Get the value potencia
 */
public function getPotencia(float $potencia): void
{
    $this->potencia = $potencia;
}
// crear un metodo getConsumo int horas que reciba una cantidad
// de horas de funcionamiento y devuelva el consumo en KW consumidos
// por el electrodoméstico en ese tiempo

public function getConsumo(int $horas): int
{
    return $this->potencia * $horas;
}
// crear un metodo getCosteConsumo que reciba una cantidad de horas así como el precio
// de kW por hora y devuelve el coste total del consumo por el electrodoméstico en ese tiempo

public function getCosteConsumo(int $horas, float $precio): float
{
    return $this->potencia * $horas * $precio;
}
}