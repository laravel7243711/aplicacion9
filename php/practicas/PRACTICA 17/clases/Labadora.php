<?php

namespace clases;

class Labadora extends Electrodomestico
{
    public float $precio = 0;
    public bool $aguaCaliente = true;
    public int $horas;
    public float $costeHora;
    // hacer un constructor que como parametros su marca, precio y potencia
    // y su modo de funcionamiento de agua fria o caliente
    public function __construct(string $marca, float $precio, float $potencia, bool $aguaCaliente)
    {
        $this->marca = $marca;
        $this->potencia = $potencia;
        $this->precio = $precio;
        $this->aguaCaliente = $aguaCaliente;
    }



    /**
     * Get the value of aguaCaliente
     */
    public function getAguaCaliente(): bool
    {
        return $this->aguaCaliente;
    }

    /**
     * Set the value of aguaCaliente
     */
    public function setAguaCaliente(): bool
    {
        return $this->aguaCaliente;
    }
// hacer un metodo toString que devuelva todas las características de la lavadora
/**
 * Get all the characteristics of the washing machine as a string
 */
public function __toString(): string
{
    return "Marca: " . $this->marca . "<br>"
         . "Precio: " . $this->precio . "<br>"
         . "Potencia: " . $this->potencia . "<br>"
         . "Agua Caliente: " . ($this->aguaCaliente ? "Sí" : "No") . "<br>";
}
// crear un metodo getConsumo (int horas double costeHora)
// este metodo no necesita ser sobreescrito 
// por lo que deberia funcionar correctamente
public function getConsumo(int $horas, float $costeHora): float
{
    return "Consumo:" . $this->potencia * $horas * $costeHora;

}
}
