<?php
// inicializo las variables
$num1 = 0;
$num2 = 0;
$salida = "";

// leo los datos del formulario
$num1 = $_GET["numero1"];
$num2 = $_GET["numero2"];

if ($num1 > $num2) {
    $salida = "El numero1 {$num1} es mayor que el numero2 {$num2}";
} else {
    $salida = "El numero2 {$num2} es mayor que el numero1 {$num1}";
}

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <div>
        <?= $salida ?>
    </div>
</body>

</html>