<?php
function countLetterA($text) {
        $countA = 0;
        for ($i = 0; $i < strlen($text); $i++) {
            if ($text[$i] == 'a' || $text[$i] == 'A') {
                $countA++;
            }
        }
        return $countA;
    }
    function countLetterB($text) {
        $countB = 0;
        for ($i = 0; $i < strlen($text); $i++) {
            if ($text[$i] == 'b' || $text[$i] == 'B') {
                $countB++;
            }
        }
        return $countB;
    }
    function countLetterC($text) {
        $countC = 0;
        for ($i = 0; $i < strlen($text); $i++) {
            if ($text[$i] == 'c' || $text[$i] == 'C') {
                $countC++;
            }
        }
        return $countC;
    }