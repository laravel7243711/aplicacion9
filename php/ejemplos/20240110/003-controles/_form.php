<form method="post">
    <div>
        <label for="nombre">Nombre</label>
        <input type="text" id="nombre" name="nombre" placeholder="Introduce nombre" title="Introduce el nombre" required>
    </div>
    <div>
        <label for="apellidos">Apellidos</label>
        <input type="text" id="apellidos" name="apellidos" placeholder="Introduce apellidos" title="Introduce los apellidos" required>
    </div>
    <div>
        <button>Enviar</button>
    </div>

</form>