<?php

$a=[
    "nombre"    => "Juan",
    "apellidos" => "Peña",
];
// esta función tiene peligro
// no usar con get  con post 
// crea una variable con el array
// depende de como esté definido el array
// lo devuelve como un string
$b=var_export($a,true);

// convierte un array a un string con un caracter de separación
// te coge los valores no los indices
$c=implode(",",$a);
var_dump ($c);

// crear variables con los indices del array asociativo

extract($a);
var_dump ($nombre);
var_dump ($apellidos);

// otra manera
extract($a,EXTR_PREFIX_ALL,"alpe");

// se puede hacer un foreach
// realiza lo mismo que la función extract
foreach($a as $key=>$value){
    // esto es equivalente a
    // $key="alpe_.$key";
    $$key=$value;
}


var_dump ($alpe_nombre);
var_dump($alpe_apellidos);

?>