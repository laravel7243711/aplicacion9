<?php
$datos = [
    "nombre" => "Pepe",
    "apellidos" => "Gomez",
    "edad" => null
];

// utilizando el operador if/else
// junto con la funcion isset
// realizar las siguientes asignaciones
// $nombre = "Pepe";
// $apellidos = "no conocidos";
// $edad = "no conocida";

if (isset($datos["nombre"])) {
    $nombre = $datos["nombre"];
} else {
    $nombre = "no conocido";
}

if (isset($datos["apellidos"])) {
    $apellidos = $datos["apellidos"];
} else {
    $apellidos = "no conocidos";
}

if (isset($datos["edad"])) {
    $edad = $datos["edad"];
} else {
    $edad = "no conocida";
}

var_dump($nombre, $apellidos, $edad);

// quiero realizar lo mismo pero utilizando ternario
$nombre = (isset($datos["nombre"])) ? $datos["nombre"] : "no conocido";
$apellidos = (isset($datos["apellidos"])) ? $datos["apellidos"] : "no conocidos";
$edad = (isset($datos["edad"])) ? $datos["edad"] : "no conocida";

var_dump($nombre, $apellidos, $edad);


