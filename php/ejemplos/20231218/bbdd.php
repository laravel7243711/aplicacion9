<?php

$alumnos = [
    [
        "id" => 1,
        "nombre" => "Pepe",
        "apellidos" => "Perez",
        "edad" => 20
    ],
    [
        "id" => 2,
        "nombre" => "Juan",
        "apellidos" => "Lopez",
        "edad" => 21
    ]
];

// creo un array bidimensional conectandome a 
// un servidor mysql y lanzando una consulta
$conexion = mysqli_connect("localhost", "root", "", "alumnos");
$datos = $conexion
    ->query("select * from alumno")
    ->fetch_all(MYSQLI_ASSOC);



$menu = [
    [
        "Home" => "www.alpeformacion.es",
        "Donde estamos" => "pepito.php",
        "Quienes somos" => "#",
        "mas informacion" =>
        [
            "Contacto" => "contacto.php",
            "Alumnos" => "alumnos.php"
        ]
    ]
];



// <ul>
//     <li><a href="www.alpeformacion.es">Home</a></li>
//     <li><a href="pepito.php">Donde estamos</a></li>
//     <li><a href="#">Quienes somos</a></li>
//     <li>Mas informacion
//         <ul>
//             <li><a href="contacto.php">Contacto</a></li>
//             <li><a href="alumnos.php">Alumnos</a></li>
//         </ul>
//     </li>
// </ul>