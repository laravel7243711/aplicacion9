<?php

class Coche
{
    // crear los atributos
    private string $color;
    private int $numeroPuertas;
    private string $marca;
    private int $gasolina;

    // creo el constructor
    public function __construct(
        string $color = "",
        int $numeroPuertas = 0,
        string $marca = "",
        int $gasolina = 0
    ) {
        $this
            ->setColor($color)
            ->setNumeroPuertas($numeroPuertas)
            ->setMarca($marca)
            ->setGasolina($gasolina);
    }

    // crear los metodos
    // metodo llenar tanque
    // toma un parámetro gasolina_nueva para agregar gasolina al tanque

    public function llenarTanque(int $gasolinaNueva): void
    {
        $this->gasolina += $gasolinaNueva;
    }

    // metodo acelerar
    // que resta una unidad de gasolina y devuelve la cantidad restante.

    public function acelerar(): int
    {
        $this->gasolina -= 1;
        return $this->gasolina;
    }

    /**
     * Get the value of gasolina
     *
     * @return int
     */
    public function getGasolina(): int
    {
        // devuelve la gasolina
        return $this->gasolina;
    }

    /**
     * Set the value of gasolina
     *
     * @param int $gasolina
     *
     * @return self
     */
    public function setGasolina(int $gasolina): self
    {
        $this->gasolina = $gasolina;

        return $this;
    }

    /**
     * Get the value of marca
     *
     * @return string
     */
    public function getMarca(): string
    {
        return $this->marca;
    }

    /**
     * Set the value of marca
     *
     * @param string $marca
     *
     * @return self
     */
    public function setMarca(string $marca): self
    {
        $this->marca = $marca;

        return $this;
    }

    /**
     * Get the value of numeroPuertas
     *
     * @return int
     */
    public function getNumeroPuertas(): int
    {
        return $this->numeroPuertas;
    }

    /**
     * Set the value of numeroPuertas
     *
     * @param int $numeroPuertas
     *
     * @return self
     */
    public function setNumeroPuertas(int $numeroPuertas): self
    {
        $this->numeroPuertas = $numeroPuertas;

        return $this;
    }

    /**
     * Get the value of color
     *
     * @return string
     */
    public function getColor(): string
    {
        return $this->color;
    }

    /**
     * Set the value of color
     *
     * @param string $color
     *
     * @return self
     */
    public function setColor(string $color): self
    {
        $this->color = $color;
        return $this;
    }
}
