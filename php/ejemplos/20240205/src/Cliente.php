<?php

namespace src;

class Cliente extends Persona
{
    private ?string $nombreEmpresa;
    private ?string $telefono;

    // sobreescribir mostrar
    public function mostrar(): string
    {
        $salida = "<ul>";
        $salida .= "<li> Nombre: " . $this->getNombre() . "</li>";
        $salida .= "<li> Edad: " . $this->getEdad() . "</li>";
        $salida .= "<li> Telefono: " . $this->telefono . "</li>";
        $salida .= "<li> Nombre empresa: " . $this->nombreEmpresa . "</li>";
        $salida .= "</ul>";
        return $salida;
    }

    public function __construct()
    {
        parent::__construct();
        $this->telefono = null;
        $this->nombreEmpresa = null;
    }



    /**
     * Get the value of nombreEmpresa
     *
     * @return ?string
     */
    public function getNombreEmpresa(): ?string
    {
        return $this->nombreEmpresa;
    }

    /**
     * Set the value of nombreEmpresa
     *
     * @param ?string $nombreEmpresa
     *
     * @return self
     */
    public function setNombreEmpresa(?string $nombreEmpresa): self
    {
        $this->nombreEmpresa = $nombreEmpresa;

        return $this;
    }

    /**
     * Get the value of telefono
     *
     * @return ?string
     */
    public function getTelefono(): ?string
    {
        return $this->telefono;
    }

    /**
     * Set the value of telefono
     *
     * @param ?string $telefono
     *
     * @return self
     */
    public function setTelefono(?string $telefono): self
    {
        $this->telefono = $telefono;

        return $this;
    }
}
