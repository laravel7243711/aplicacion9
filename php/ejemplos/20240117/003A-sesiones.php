<?php
// inicializo la sesion
session_start();
// quiero un formulario que me permita introducir un numero
// y quiero que me muestre todos los numeros introducidos
// quiero realizar el ejercicio con sesiones

// comprobar si existe la variable de session
if (!isset($_SESSION['numero'])) {
    // si no existe la variable de session
    // la creo vacia
    $_SESSION['numero'] = [];
}

// he pulsado el boton de enviar
if ($_POST) {
    array_push($_SESSION["numero"], $_POST["numero"]);
    // $_SESSION["numero"][] = $_POST["numero"];
}

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <form method="post">
        <div>
            <label for="numero">Numero</label>
            <input type="number" id="numero" name="numero">
        </div>
        <div>
            <button type="submit">Enviar</button>
        </div>
    </form>
    <?php
    foreach ($_SESSION["numero"] as $num) {
        echo $num . "<br>";
    }
    ?>
</body>

</html>