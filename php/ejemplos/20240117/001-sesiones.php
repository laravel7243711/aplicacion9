<?php
// arranco la sesion
session_start();

// Crear un formulario que me permita introducir un numero
// almacenar ese numero en una variable de sesion

// has pulsado el boton de enviar
if ($_POST) {
    $_SESSION['numero'] = $_POST['numero'];
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <form method="post">
        <div>
            <label for="numero">Numero</label>
            <input type="number" id="numero" name="numero">
        </div>
        <div>
            <button type="submit">Enviar</button>
        </div>
    </form>
</body>

</html>