<?php
// arranco la sesion
session_start();

// mostrar el ultimo numero introducido en el formulario del ejercicio 1
// el numero almacenado en la variable de sesion

// inicializo la variable
$numero = "";

// leer de la sesion el numero
if (isset($_SESSION['numero'])) {
    $numero = $_SESSION['numero'];
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <div>
        Numero : <?= $numero ?>
    </div>
</body>

</html>