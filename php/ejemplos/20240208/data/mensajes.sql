﻿DROP DATABASE IF EXISTS  mensajes;
CREATE DATABASE  mensajes;

USE mensajes;
CREATE TABLE mensaje (
ID int AUTO_INCREMENT PRIMARY KEY,
nombre varchar (100),
mensaje varchar(500),
fecha datetime);

                       