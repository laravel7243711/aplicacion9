﻿/*

*/
-- todo esto son instruciones de definicion de datos
DROP DATABASE IF EXISTS ejemploldd;
CREATE DATABASE IF NOT EXISTS ejemploLDD;
USE ejemploLDD;

CREATE TABLE productos(
-- creamos los campos
 id int AUTO_INCREMENT,
 nombre varchar(200),
 precio float,
-- creamos la clave principal
 PRIMARY KEY (id)
 -- creamos otras claves
 -- creamos checks y aserciones
);
-- insertar un par de registros

-- instruciones de manipulacion de datos
INSERT INTO productos 
( nombre, precio)   VALUES
 ( 'lapiz', 2.4),
 ('boli',1.8);

-- listar
SELECT 
 * 
 
 FROM productos p;
