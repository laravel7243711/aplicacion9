<?php
// inicializar las variables
$resultado = "";

// controlamos si he pulsado el boton de enviar
if ($_POST) {
    // leer los datos del formulario
    $resultado = implode(",", $_POST["opciones"]);
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <form method="post">

        <select name="opciones[]" multiple>
            <option disabled selected>Elige un medio de transporte</option>
            <option value="car">Coche</option>
            <option value="bike">Bicicleta</option>
            <option value="motorbike">Moto</option>
        </select>

        <button type="submit">Enviar</button>
    </form>
    <?= $resultado ?>
</body>

</html>