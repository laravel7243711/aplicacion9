<?php

// Crear un par de variables 

// minimo=4
// Maximo=7
// imprimir una lista desde minimo hasta maximo
// - 4
// - 5
// - 6
// - 7

$minimo = 4;
$maximo = 7;
echo "<ul>";
for ($contador = $minimo; $contador <= $maximo; $contador++) {
    echo "<li>{$contador}</li>";
}
echo "</ul>";

?>

<ul>
    <?php
    for ($contador = $minimo; $contador <= $maximo; $contador++) {
    ?>
        <li><?= $contador ?></li>
    <?php
    }
    ?>
</ul>


<?php
$salida = "";
$salida = "<ul>";
for ($contador = $minimo; $contador <= $maximo; $contador++) {
    $salida .= "<li>{$contador}</li>";
}
$salida .= "</ul>";
echo $salida;
?>