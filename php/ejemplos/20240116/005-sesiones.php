<?php
// quiero un contador de visitas que cuando llegue a 10 se para
session_start();

// compruebo si existe la variable de sesion
if (isset($_SESSION['visitas'])) {
    $_SESSION['visitas']++;
} else {
    $_SESSION['visitas'] = 1;
}

echo $_SESSION['visitas'];

if ($_SESSION['visitas'] == 10) {
    // cerrar la sesion en este punto 
    // y borra todas las variables de sesion
    session_unset();
}
echo "<br>";
echo $_SESSION['visitas'];
