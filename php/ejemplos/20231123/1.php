<?php

// creo y asigno valor a la variable
$texto = "ejemplo de clase";
$a = 10;
$b = 2.34;
$c = true;
$d = null;
$e = "";
$f;


// mostrando la variable
echo $texto;

// depurar la variable (valor y tipo)
var_dump("texto=", $texto);

// depurar todas las variables
var_dump($a, $b, $c, $d, $e, $f);
