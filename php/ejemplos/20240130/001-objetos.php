<?php
// cargo la clase en el espacio de nombres actual
use clases\personas\Tecnicos;

require_once "autoload.php";

$tecnico = new Tecnicos("juan");
$tecnico->horas = 100;
var_dump($tecnico);

// podemos utilizar getter y setter para 
// iniciales
// $tecnico->setIniciales("jj");
// echo $tecnico->getIniciales();

// podemos colocar despues del setter otro miembro
// porque esta en fluent

echo $tecnico
    ->setIniciales("JJ")
    ->getIniciales();

echo $tecnico->getSueldo();
