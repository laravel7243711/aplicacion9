<?php
// variables a utilizar
// variables con los datos introducidos
$a = 10;
$b = 3;
// variable para almacenar los resultados
$resultados = [];
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Operadores</title>
</head>

<body>
    <?php
    // Quiero que coloquemos los resultados de las siguientes operaciones
    // a+b
    // a-b
    // a*b
    // a resto b
    // a elevado b
    // a dividido entre b
    // a es igual a b
    // a menor o igual que b

    // colocamos una tabla en donde la primera columa es el nombre
    // de la operacion
    // en la segunda columna el resultado

    // procesamiento

    // inicializar el array
    // $resultados = [
    //     "suma" => $a + $b,
    //     "resta" => $a - $b,
    //     "producto" => $a * $b,
    //     "resto" => $a % $b,
    //     "elevado" => $a ** $b,
    //     "cociente" => $a / $b,
    // ];

    // añadir elementos al array
    // push
    $resultados["suma"] = $a + $b;
    $resultados["resta"] = $a - $b;
    $resultados["producto"] = $a * $b;
    $resultados["resto"] = $a % $b;
    $resultados["elevado"] = $a ** $b;
    $resultados["cociente"] = $a / $b;


    if ($a == $b) {
        $resultados["iguales"] = "Si, son iguales";
    } else {
        $resultados["iguales"] = "No son iguales";
    }

    if ($a <= $b) {
        $resultados["menores"] = "a es menor o igual que b";
    } else {
        $resultados["menores"] = "a no es menor o igual que b";
    }

    // mostrar resultados
    // var_dump($resultados);
    ?>
    <table border="1">
        <tr>
            <td>Operaciones</td>
            <td>Resultados</td>
        </tr>
        <tr>
            <td>Suma</td>
            <td><?php echo $resultados["suma"] ?></td>
        </tr>
        <tr>
            <td>Restar</td>
            <td><?= $resultados["resta"] ?></td>
        </tr>
        <tr>
            <td>Producto</td>
            <td><?= $resultados["producto"] ?></td>
        </tr>
        <tr>
            <td>Resto</td>
            <td><?= $resultados["resto"] ?></td>
        </tr>
        <tr>
            <td>Elevado</td>
            <td><?= $resultados["elevado"] ?></td>
        </tr>
        <tr>
            <td>Cociente</td>
            <td><?= $resultados["cociente"] ?></td>
        </tr>
        <tr>
            <td>A igual a B</td>
            <td>
                <?= $resultados["iguales"] ?>
            </td>
        </tr>
        <tr>
            <td>A menor o igual que B</td>
            <td>
                <?= $resultados["menores"] ?>
            </td>
        </tr>
    </table>
</body>

</html>